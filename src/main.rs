mod interactions;
use crate::interactions::*;

use anyhow::Result;
use pixels::{Pixels, SurfaceTexture};

use winit::dpi::LogicalSize;
use winit::event::{Event, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Fullscreen, WindowBuilder};

use winit_input_helper::WinitInputHelper;

// The scaling factor (number of cells per pixel)
const SCALE: f32 = 1.;

// The initial rates
const PREDATION_RATE: i32 = 512;
const SWAP_RATE: i32 = 512;
const REPRODUCTION_RATE: i32 = 512;

// Amount to change the rates on each key press
const RATE_CHANGE: i32 = 64;

// Don't change these
const WIDTH: f32 = 1920. * SCALE;
const HEIGHT: f32 = 1080. * SCALE;

fn main() -> Result<()> {
    env_logger::init();
    let event_loop: EventLoop<()> = EventLoop::new();

    let mut input = WinitInputHelper::new();

    let window = {
        let size = LogicalSize::new(WIDTH, HEIGHT);
        WindowBuilder::new()
            .with_title("Hello Pixels")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .with_fullscreen(Some(Fullscreen::Borderless(None)))
            .build(&event_loop)
            .unwrap()
    };

    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(WIDTH as u32, HEIGHT as u32, surface_texture)?
    };
    let mut world = RockPaperScissorsGame::new(
        HEIGHT as usize,
        WIDTH as usize,
        PREDATION_RATE,
        SWAP_RATE,
        REPRODUCTION_RATE,
    );

    // Draw initial points in random positions
    world.draw_random_point(CellColor::Blue)?;
    world.draw_random_point(CellColor::Red)?;
    world.draw_random_point(CellColor::Yellow)?;

    // // Find the number of threads available on the machine (default is 8)
    // let no_threads = thread::available_parallelism().unwrap_or(NO_THREADS_DEFAULT.try_into()?);

    // Color to draw with
    let mut draw_color = CellColor::Black;

    world.iterate_parallel_forever();

    // Start the event loop for the window
    event_loop.run(move |event, _, control_flow: &mut ControlFlow| {
        // Draw the current frame
        if let Event::RedrawRequested(_) = event {
            world.draw(pixels.frame_mut());
            if pixels.render().is_err() {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        // Handle input events
        if input.update(&event) {
            // Quit on Q or or OS request
            if input.key_pressed_os(VirtualKeyCode::Q) || input.close_requested() {
                *control_flow = ControlFlow::Exit;
                return;
            }

            // Draw on the screen
            if input.mouse_held(0) {
                if let Some((x, y)) = input.mouse() {
                    let (row, col) = ((y * SCALE) as usize, (x * SCALE) as usize);
                    world[(row, col)] = draw_color;
                }
            }

            // Set colors to draw with
            if input.key_pressed_os(VirtualKeyCode::R) {
                draw_color = CellColor::Red;
            }
            if input.key_pressed_os(VirtualKeyCode::Y) {
                draw_color = CellColor::Yellow;
            }
            if input.key_pressed_os(VirtualKeyCode::B) {
                draw_color = CellColor::Blue;
            }
            if input.key_pressed_os(VirtualKeyCode::K) {
                draw_color = CellColor::Black;
            }

            // handle changes to rates
            if input.key_pressed_os(VirtualKeyCode::A) {
                let _ = world.change_predation_rate(RATE_CHANGE);
            }
            if input.key_pressed_os(VirtualKeyCode::Z) {
                let _ = world.change_predation_rate(-RATE_CHANGE);
            }
            if input.key_pressed_os(VirtualKeyCode::S) {
                let _ = world.change_swap_rate(RATE_CHANGE);
            }
            if input.key_pressed_os(VirtualKeyCode::X) {
                let _ = world.change_swap_rate(-RATE_CHANGE);
            }
            if input.key_pressed_os(VirtualKeyCode::D) {
                let _ = world.change_reproduction_rate(RATE_CHANGE);
            }
            if input.key_pressed_os(VirtualKeyCode::C) {
                let _ = world.change_reproduction_rate(-RATE_CHANGE);
            }

            // Resize the window
            if let Some(size) = input.window_resized() {
                if pixels.resize_surface(size.width, size.height).is_err() {
                    *control_flow = ControlFlow::Exit;
                    return;
                }
            }

            // Update internal state and request a redraw
            window.request_redraw();
        }
    });
}
