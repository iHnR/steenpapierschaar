use anyhow::{anyhow, Result};
use core::slice;
use derive_more::{Index, IndexMut};
use jaaptools::ops::BoundedAdd;
use killable_thread::StoppableHandle;
use log::error;
use ndarray::{Array2, Dim};
use rand::{distributions::Standard, prelude::*};
use std::sync::{
    atomic::{AtomicI32, Ordering},
    Arc,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CellColor {
    Black,
    Red,
    Yellow,
    Blue,
}

impl CellColor {
    #[inline]
    pub fn can_eat(&self, other: &Self) -> bool {
        use CellColor::*;
        match (*self, *other) {
            (Red, Yellow) | (Yellow, Blue) | (Blue, Red) => true,
            _ => false,
        }
    }

    #[inline]
    pub fn to_rgba(&self) -> [u8; 4] {
        use CellColor::*;
        match self {
            Red => [0xea, 0x2d, 0x2d, 0xff],
            Yellow => [0xfc, 0xe9, 0x2b, 0xff],
            Blue => [0x33, 0x3b, 0x98, 0xff],
            Black => [0x0, 0x0, 0x0, 0x0],
        }
    }
}

impl Default for CellColor {
    fn default() -> Self {
        Self::Black
    }
}

impl Distribution<CellColor> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> CellColor {
        use CellColor::*;
        match rng.gen_range(0..3) {
            0 => Red,
            1 => Yellow,
            2 => Blue,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug)]
pub enum Interaction {
    Predate,
    Swap,
    Reproduce,
    Nothing,
}

#[derive(Index, IndexMut)]
pub struct RockPaperScissorsGame {
    // Size
    height: usize,
    width: usize,

    // Rates
    predation_rate: Arc<AtomicI32>,
    swap_rate: Arc<AtomicI32>,
    reproduction_rate: Arc<AtomicI32>,

    // Internal representation
    #[index]
    #[index_mut]
    board: Array2<CellColor>,

    // Handles for possible threads
    handles: Option<Vec<StoppableHandle<()>>>,
}

impl Drop for RockPaperScissorsGame {
    fn drop(&mut self) {
        if let Some(handles) = self.handles.take() {
            let handles: Vec<_> = handles.into_iter().map(|h| h.stop().join()).collect();
            if handles.iter().any(|h| h.is_err()) {
                error!("An error happened joining one of the threads!");
            }
        }
    }
}

impl RockPaperScissorsGame {
    /// Create a new Game
    pub fn new(
        height: usize,
        width: usize,
        predation_rate: i32,
        swap_rate: i32,
        reproduction_rate: i32,
    ) -> Self {
        RockPaperScissorsGame {
            height,
            width,
            predation_rate: Arc::new(AtomicI32::new(predation_rate)),
            swap_rate: Arc::new(AtomicI32::new(swap_rate)),
            reproduction_rate: Arc::new(AtomicI32::new(reproduction_rate)),
            board: Array2::default(Dim([height, width])),
            handles: None,
        }
    }

    pub fn change_predation_rate(&mut self, diff: i32) -> Result<()> {
        self.predation_rate
            .fetch_update(Ordering::Release, Ordering::Acquire, |x| {
                Some(x.bounded_add(diff, 0, 1024))
            })
            .map_err(|_| anyhow!("not update well"))?;
        Ok(())
    }
    pub fn change_swap_rate(&mut self, diff: i32) -> Result<()> {
        self.swap_rate
            .fetch_update(Ordering::Release, Ordering::Acquire, |x| {
                Some(x.bounded_add(diff, 0, 1024))
            })
            .map_err(|_| anyhow!("not update well"))?;
        Ok(())
    }
    pub fn change_reproduction_rate(&mut self, diff: i32) -> Result<()> {
        self.reproduction_rate
            .fetch_update(Ordering::Release, Ordering::Acquire, |x| {
                Some(x.bounded_add(diff, 0, 1024))
            })
            .map_err(|_| anyhow!("not update well"))?;
        Ok(())
    }

    /// Draw a point of the specified color in a random spot
    pub fn draw_random_point(&mut self, color: CellColor) -> Result<()> {
        let mut rng = rand::thread_rng();
        let cell = self
            .board
            .as_slice_mut()
            .ok_or(anyhow!("Board is not contiguous!"))?
            .choose_mut(&mut rng)
            .ok_or(anyhow!("Board is empty!"))?;
        *cell = color;
        Ok(())
    }

    /// This iterates the board forever in the specified number of threads
    /// Technically unsafe since data races can occur.
    /// Uses the transmitter to transmit raw pointers to the board and rate variables.
    /// These come as usize since it is not possible to transmit raw pointers.
    pub fn iterate_parallel_forever(&mut self) {
        let no_threads: usize = std::thread::available_parallelism()
            .map(|x| x.into())
            .unwrap_or(4);

        self.handles = Some(
            (0..no_threads)
                .map(|_| {
                    let slice = unsafe {
                        slice::from_raw_parts_mut(self.board.as_mut_ptr(), self.board.len())
                    };
                    let mut rng = rand_xoshiro::Xoshiro256Plus::seed_from_u64(rand::random());

                    let width = self.width;
                    let height = self.height;

                    let predation_rate = self.predation_rate.clone();
                    let swap_rate = self.swap_rate.clone();
                    let reproduction_rate = self.reproduction_rate.clone();

                    killable_thread::spawn(move |stopped| {
                        // Convenience includes
                        use CellColor::*;
                        use Interaction::*;
                        while !stopped.get() {
                            // This only happens once evry 2^10 cycles since atomic loads are
                            // expensive
                            let cached_predation_rate = predation_rate.load(Ordering::Relaxed);
                            let cached_swap_rate = swap_rate.load(Ordering::Relaxed);
                            let cached_reproduction_rate =
                                reproduction_rate.load(Ordering::Relaxed);

                            for _ in 0..1024 {
                                // Get two random neighbouring indices
                                let mut row = rng.gen_range(0..height);
                                let mut col = rng.gen_range(0..width);
                                let i = row * width + col;
                                match rng.gen_bool(0.5) {
                                    true => row = (row + 1).rem_euclid(height),
                                    false => col = (col + 1).rem_euclid(width),
                                }

                                // Coninue if the cells are the same color
                                let j = row * width + col;
                                if slice[i] == slice[j] {
                                    continue;
                                }

                                // Generate a random action
                                let action = [Predate, Reproduce, Swap]
                                    .choose_weighted(&mut rng, |a| match a {
                                        Predate => cached_predation_rate,
                                        Swap => cached_swap_rate,
                                        Reproduce => cached_reproduction_rate,
                                        Nothing => 0,
                                    })
                                    .unwrap_or(&Nothing);

                                // Perform the action
                                match *action {
                                    Predate => {
                                        if slice[i].can_eat(&slice[j]) {
                                            slice[j] = Black;
                                        } else if slice[j].can_eat(&slice[i]) {
                                            slice[i] = Black;
                                        }
                                    }
                                    Swap => slice.swap(i, j),
                                    Reproduce => match (slice[i], slice[j]) {
                                        (Black, color) => slice[i] = color,
                                        (color, Black) => slice[j] = color,
                                        _ => {}
                                    },
                                    Nothing => {}
                                }
                            }
                        }
                    })
                })
                .collect(),
        );
    }

    /// Draw the board to a `pixels` framebuffer
    #[inline]
    pub fn draw(&self, frame: &mut [u8]) {
        for (cell, pixel) in self.board.iter().zip(frame.chunks_exact_mut(4)) {
            pixel.copy_from_slice(&cell.to_rgba());
        }
    }
}
