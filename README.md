# Rock Paper Scisoors Simulation

This is a simple program that simulates a predation model inspired by the rock paper scissors game.

# Rules

There are 3 interactions between cells:

1. Eat
   - Red can eat Yellow
   - Yellow can eat Blue
   - Blue can eat Red
2. Swap
   Any two cells can swap
3. Reproduce
   If the neighbouring cell is dead, the cell can copy itself to that cell.

# Mechanics

The program repeatedly does the following to simulate:

1. Pick a random index from a uniform distribution.
2. Pick a random direction from a uniform distribution.
3. Pick a random inderaction weighted by the probabilities of each.
4. Perform the interaction if it is possible.

# Controls

- AZ controls the Predation rate
- SX controls the Swap rate
- DC controls the Reproduction rate
- RYBK sets the current draw color
- You can draw colors when with the mouse

# Caveats

- The program uses unsafe access to variables from multiple threads. This can (and probably will) result in data races in rare circumstances. This could cause some undesired behaviour such as a cell eating itself when another thread was trying to swap it. This does not really affect the outcome so it's allowed in the name of performance.
- There is currently no way to lower the speed of the simulation, so it might run too fast to observer properly. It mostly runs too slow though.
- scaling is a bit funky and can cause some undefined behaviour in drawing.
